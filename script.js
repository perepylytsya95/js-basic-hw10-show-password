//1)В файле index.html лежит разметка для двух полей ввода пароля.
//2)По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
//  В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
//3)Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
//4)Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
//5)По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
//6)Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
//7)Если значение не совпадают - вывести под вторым полем текст красного цвета Нужно ввести одинаковые значения
//8)После нажатия на кнопку страница не должна перезагружаться
//9)Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

let topInput = document.querySelector('[data-name = topInput]');
let bottomInput = document.querySelector('[data-name = bottomInput]');
let topEye = document.querySelector('[data-name = topEye1]');
let bottomEye = document.querySelector('[data-name = bottomEye1]');
let confirmBtn = document.querySelector('.btn');
let passwordError = document.createElement('p');
passwordError.innerText = "Введенные значения должны быть одинаковыми, поля ввода и подтверждения пароля не могут быть пустыми.";
passwordError.style.color = "red";


function handlerTop (){
    if (topInput.type === "password"){
        topInput.type = 'null';
        topEye.className = "fas fa-eye-slash icon-password";
    }  else {
        topInput.type = 'password';
        topEye.className = 'fas fa-eye icon-password';
    }
}

function handlerBottom (){
    if (bottomInput.type === "password"){
        bottomInput.type = 'null';
        bottomEye.className = "fas fa-eye-slash icon-password";
    }  else {
        bottomInput.type = 'password';
        bottomEye.className = 'fas fa-eye icon-password';
    }
}

topEye.addEventListener('click', handlerTop);
bottomEye.addEventListener('click', handlerBottom);

confirmBtn.addEventListener('click', event =>{
    event.preventDefault();
    if (topInput.value === bottomInput.value && topInput.value.trim() !== "" && bottomInput.value.trim() !== ""){
        passwordError.remove();
        alert('Welcome');
    } else {
        confirmBtn.before(passwordError);
    }
})